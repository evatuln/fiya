package com.example.gismobile;

import androidx.fragment.app.FragmentActivity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng untad = new LatLng(-7.403948148006805, 109.24634061266917);
        LatLng vatulemo = new LatLng(-7.417931690018637, 109.25393725413451);
        //custom
        int tinggi = 100;
        int lebar = 100;
        //BitmapDrawable bitmapStart = (BitmapDrawable)getResources().getDrawable(R.drawable.pin_map_hitam);
        //BitmapDrawable bitmapDes = (BitmapDrawable) getResources().getDrawable(R.drawable.pin_map_merah);
        //Bitmap s = bitmapStart.getBitmap();
        //Bitmap d = bitmapDes.getBitmap();
        //Bitmap markerStart = Bitmap.createScaledBitmap(s, lebar, tinggi, false);
        //Bitmap markerDes = Bitmap.createScaledBitmap(d, lebar, tinggi, false);

        //add marker to map
        mMap.addMarker(new MarkerOptions().position(untad).title("Marker in Universitas Jendral Soedirman")
                .snippet("ini adalah kampus kami")
                );

        mMap.addMarker(new MarkerOptions().position(vatulemo).title("Marker in Apotik Nitro Farma")
                .snippet("ini adalah Apotik Nitro Farma")
                );

        mMap.addPolyline(new PolylineOptions().add(
                untad,
                new LatLng(-7.404649904421444, 109.24628188844144),
                new LatLng(-7.40469881924945, 109.2452980323522),
                new LatLng(-7.40567376250324, 109.24536197662087),
                new LatLng(-7.405705467938712, 109.24523408808352),
                vatulemo
                ).width(10)
                .color(Color.BLUE)
        );

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(vatulemo, 14.15f));

    }
}